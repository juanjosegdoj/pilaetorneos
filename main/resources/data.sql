insert into genero_tbl(nombre) values("Masculino");
insert into genero_tbl(nombre) values("Femenino");
insert into genero_tbl(nombre) values("Mixto");

insert into deporte_tbl(nombre, descripcion) values("Fútbol", "Es un deporte de equipo jugado entre dos conjuntos de once jugadores cada uno");
insert into deporte_tbl(nombre, descripcion) values("Baloncesto", "Es un deporte de equipo, jugado entre dos conjuntos de cinco jugadores cada uno durante cuatro períodos o cuartos de diez2​ o doce minutos cada uno");
insert into deporte_tbl(nombre, descripcion) values("Voleibol", "Es un deporte que se juega con una pelota y en el que dos equipos, integrados por seis jugadores cada uno, se enfrentan sobre un área de juego separada por una red central.");

