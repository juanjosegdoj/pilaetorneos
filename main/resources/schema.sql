SET FOREIGN_KEY_CHECKS=0
;

/* Drop Tables */

DROP TABLE IF EXISTS `deporte_tbl` CASCADE
;

DROP TABLE IF EXISTS `equipo_jugador` CASCADE
;

DROP TABLE IF EXISTS `equipo_tbl` CASCADE
;

DROP TABLE IF EXISTS `genero_tbl` CASCADE
;

DROP TABLE IF EXISTS `jugador_tbl` CASCADE
;

DROP TABLE IF EXISTS `torneo_tbl` CASCADE
;

/* Create Tables */

CREATE TABLE `deporte_tbl`
(
    `id_deporte` INT NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(50) NOT NULL,
    `descripcion` VARCHAR(200) 	 NULL,
    CONSTRAINT `PK_deporte_tbl` PRIMARY KEY (`id_deporte` ASC)
)

;

CREATE TABLE `equipo_jugador`
(
    `id_equipo` INT NOT NULL,
    `id_jugador` INT NOT NULL
)

;

CREATE TABLE `equipo_tbl`
(
    `id_equipo` INT NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(50) NOT NULL,
    `id_torneo` INT NOT NULL,
    CONSTRAINT `PK_equipo` PRIMARY KEY (`id_equipo` ASC)
)

;

CREATE TABLE `genero_tbl`
(
    `id_genero` INT NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(50) NOT NULL,
    CONSTRAINT `PK_genero_tbl` PRIMARY KEY (`id_genero` ASC)
)

;

CREATE TABLE `jugador_tbl`
(
    `id_jugador` INT NOT NULL AUTO_INCREMENT,
    `identificacion` VARCHAR(50) NOT NULL,
    `nombre` VARCHAR(50) NOT NULL,
    `apellido` VARCHAR(50) NOT NULL,
    `fecha_nacimiento` DATE NOT NULL,
    `id_genero` INT NOT NULL,
    CONSTRAINT `PK_jugador_tbl` PRIMARY KEY (`id_jugador` ASC)
)

;

CREATE TABLE `torneo_tbl`
(
    `id_torneo` INT NOT NULL AUTO_INCREMENT,
    `nombre` VARCHAR(50) NOT NULL,
    `descripcion` VARCHAR(200) 	 NULL,
    `imagen` VARCHAR(200) 	 NULL,
    `premiacion` VARCHAR(200) 	 NULL,
    `id_genero` INT NOT NULL,
    `fecha_inicio` DATE NOT NULL,
    `max_jugadores_por_equipo` INT 	 NULL,
    `min_jugadores_por_equipo` INT 	 NULL,
    `fk_deporte` INT NOT NULL,
    `limite_fecha_nacimiento` DATE NOT NULL,
    CONSTRAINT `PK_Torneo` PRIMARY KEY (`id_torneo` ASC)
)

;

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE `equipo_jugador`
    ADD INDEX `IXFK_equipo_jugador_equipo_tbl` (`id_equipo` ASC)
;

ALTER TABLE `equipo_jugador`
    ADD INDEX `IXFK_equipo_jugador_jugador_tbl` (`id_jugador` ASC)
;

ALTER TABLE `equipo_tbl`
    ADD INDEX `IXFK_equipo_tbl_torneo_tbl` (`id_torneo` ASC)
;

ALTER TABLE `jugador_tbl`
    ADD INDEX `IXFK_jugador_tbl_genero_tbl` (`id_genero` ASC)
;

ALTER TABLE `torneo_tbl`
    ADD INDEX `IXFK_torneo_tbl_deporte_tbl` (`fk_deporte` ASC)
;

ALTER TABLE `torneo_tbl`
    ADD INDEX `IXFK_torneo_tbl_genero_tbl` (`id_genero` ASC)
;

/* Create Foreign Key Constraints */

ALTER TABLE `equipo_jugador`
    ADD CONSTRAINT `FK_equipo_jugador_equipo_tbl`
        FOREIGN KEY (`id_equipo`) REFERENCES `equipo_tbl` (`id_equipo`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `equipo_jugador`
    ADD CONSTRAINT `FK_equipo_jugador_jugador_tbl`
        FOREIGN KEY (`id_jugador`) REFERENCES `jugador_tbl` (`id_jugador`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `equipo_tbl`
    ADD CONSTRAINT `FK_equipo_tbl_torneo_tbl`
        FOREIGN KEY (`id_torneo`) REFERENCES `torneo_tbl` (`id_torneo`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `jugador_tbl`
    ADD CONSTRAINT `FK_jugador_tbl_genero_tbl`
        FOREIGN KEY (`id_genero`) REFERENCES `genero_tbl` (`id_genero`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `torneo_tbl`
    ADD CONSTRAINT `FK_torneo_tbl_deporte_tbl`
        FOREIGN KEY (`fk_deporte`) REFERENCES `deporte_tbl` (`id_deporte`) ON DELETE Restrict ON UPDATE Restrict
;

ALTER TABLE `torneo_tbl`
    ADD CONSTRAINT `FK_torneo_tbl_genero_tbl`
        FOREIGN KEY (`id_genero`) REFERENCES `genero_tbl` (`id_genero`) ON DELETE Restrict ON UPDATE Restrict
;

SET FOREIGN_KEY_CHECKS=1
;
