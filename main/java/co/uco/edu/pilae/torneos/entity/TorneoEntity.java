package co.uco.edu.pilae.torneos.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import java.util.Calendar;
import java.util.List;

@Data
@Entity
@Table(name = "torneo_tbl")
public class TorneoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_torneo")
    private Long codigo;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @ManyToOne
    @JoinColumn(name = "id_genero", nullable = false)
    private GeneroEntity genero;

    @Column(name = "fecha_inicio")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es-CO", timezone = "America/Bogota")
    private Calendar fechaInicio;

    @Column(name = "max_jugadores_por_equipo")
    private int cantMaxJugadoresPorEquipo;   // Cantidad de jugadores máximamente permitidos en un equipo perteneciente al torneo

    @Column(name = "min_jugadores_por_equipo")
    private int cantMinJugadoresPorEquipo;  // Cantidad de jugadores máximamente permitidos en un equipo perteneciente al torneo

    @JoinColumn(name = "fk_deporte", nullable = false)
    @ManyToOne(optional = false, cascade=CascadeType.MERGE)
    private DeporteEntity deporte;

    @Column(name = "limite_fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es-CO", timezone = "America/Bogota")
    private Calendar limitefechaNacimiento;  // Indica la menor edad permitida

}
