package co.uco.edu.pilae.torneos.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;

import java.util.Calendar;
import java.util.List;

@Data
@Entity
@Table(name = "jugador_tbl")
public class JugadorEntity {

    /**
     * Representa los integrantes de un equipoEntity, siendo cada uno de estos participantes de un torneo.
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_jugador")
    private Long codigo;

    @Column(name = "identificacion")
    private String identificacion;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "fecha_nacimiento")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es-CO", timezone = "America/Bogota")
    private Calendar fechaNacimiento;

    @ManyToOne
    @JoinColumn(name = "id_genero", nullable = false)
    private GeneroEntity generoEntity;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "jugadores", cascade=CascadeType.REMOVE)
    private List<EquipoEntity> equipos;

}
