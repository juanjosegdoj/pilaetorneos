package co.uco.edu.pilae.torneos.entity;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "deporte_tbl")
public class DeporteEntity {

    /**
     * Representa el DeporteEntity que se va a jugar en un torneo, además de representar el
     * deporte en el que compite un equipo
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_deporte")
    public Long codigo;

    @Column(name = "nombre", nullable = false, length = 50)
    @Size(min = 3, max = 50)
    public String nombre;

    @Column(name = "descripcion", length = 200)
    public String descripcion;

}
