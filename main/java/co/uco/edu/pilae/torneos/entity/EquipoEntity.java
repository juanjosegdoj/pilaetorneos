package co.uco.edu.pilae.torneos.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "equipo_tbl")
public class EquipoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_equipo")
    private Long codigo;

    @Size(min = 3, max = 50, message = "Tamaño de nombre no válido")
    @Column(name = "nombre")
    private String nombre;

    @ManyToOne
    @JoinColumn(name = "id_torneo")
    private TorneoEntity torneo;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "equipo_jugador",
            joinColumns = {@JoinColumn(name = "id_equipo")},
            inverseJoinColumns = {@JoinColumn(name = "id_jugador")})
    private List<JugadorEntity> jugadores = new ArrayList<>();

    public void addJugador(JugadorEntity jugador) {
        this.jugadores.add(jugador);
    }

}
