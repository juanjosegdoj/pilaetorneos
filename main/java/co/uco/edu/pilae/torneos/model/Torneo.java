package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Calendar;
import java.util.List;

import static co.uco.edu.pilae.torneos.utilitarios.cadenas.UtilText.getUtilText;
import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;
import static co.uco.edu.pilae.torneos.utilitarios.fecha.UtilDate.getUtilDate;


@Data
public class Torneo {

    private Long codigo;

    private String nombre;

    private String descripcion;

    private Genero genero;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es-CO", timezone = "America/Bogota")
    private Calendar fechaInicio;

    private int cantMaxJugadoresPorEquipo;

    private int cantMinJugadoresPorEquipo;

    private List<Equipo> equipos;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", locale = "es-CO", timezone = "America/Bogota")
    private Calendar limitefechaNacimiento;  // Indica la menor edad permitida

    private Deporte deporte;

    public void validarIntegridadTorneo(OperacionEnum operacion){

        switch (operacion){

            case CREAR:
                validarIntegridadNombre();
                genero.validarIntegridadGenero(OperacionEnum.CONSULTAR);
                validarIntervaloJugadorPorEquipo();
                validarIntegridadLimiteFechaNacimiento();
                deporte.validarIntegridadDeporte(OperacionEnum.CONSULTAR);
                validarIntegridadFechaInicio();
                break;

            case ELIMINAR:
            case CONSULTAR:
                validarIntegridadCodigo();
                break;

            case ACTUALIZAR:
                validarIntegridadCodigo();
                validarIntegridadNombre();
                genero.validarIntegridadGenero(OperacionEnum.CONSULTAR);
                validarIntervaloJugadorPorEquipo();
                validarIntegridadLimiteFechaNacimiento();
                deporte.validarIntegridadDeporte(OperacionEnum.CONSULTAR);
                break;

             default:
                 throw AplicacionExcepcion.CREAR("La operación seleccionada no se encuentra dentro " +
                                 "de las opciones", ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadFechaInicio() {
        if(getUtilObject().objetoEsNulo(fechaInicio)){
            this.fechaInicio = Calendar.getInstance();
        }
    }

    public void validarIntegridadLimiteFechaNacimiento() {

        if(getUtilObject().objetoEsNulo(limitefechaNacimiento)){
            throw AplicacionExcepcion.CREAR("El  límite de fecha de nacimiento que indica la edad máxima de" +
                    "los jugadores es un parámetro obligatorio.", ExcepcionEnum.MODELO);
        }

        if(!getUtilDate().fueHaceMasDeUnAnio(limitefechaNacimiento)){
            throw AplicacionExcepcion.CREAR("La fecha de nacimiento límite, debe marcar al menos un año" +
                    "atrás de la fecha actual.", ExcepcionEnum.MODELO);
        }
    }

    private void validarIntervaloJugadorPorEquipo() {

        if(getUtilObject().objetoEsNulo(cantMaxJugadoresPorEquipo) || getUtilObject().objetoEsNulo(cantMinJugadoresPorEquipo)){
            throw AplicacionExcepcion.CREAR("La cantidad mínima y máxima es un parámtro obligatorio");
        }
        if(cantMinJugadoresPorEquipo <= 0 || cantMaxJugadoresPorEquipo <= 0){
            throw AplicacionExcepcion.CREAR("La cantidad mínima y máxima de jugadores deber ser positiva");
        }
        if(cantMaxJugadoresPorEquipo < cantMinJugadoresPorEquipo){
            throw AplicacionExcepcion.CREAR("La cantidad mínima y máxima deben cubrir un intervalo, " +
                            "con lo cual, cantMaxJugadoresPorEquipo debe ser mayor o igual a cantMinJugadoresPorEquipo",
                    ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadCodigo(){
        if(getUtilObject().objetoEsNulo(codigo) || codigo <= 0){
            throw AplicacionExcepcion.CREAR("El código del torneo tiene que ser mayor a cero",
                    ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadNombre(){
        if(getUtilObject().objetoEsNulo(nombre)  || nombre.length() < 3){
            throw AplicacionExcepcion.CREAR("El nombre del torneo es obligatorio" +
                    "y debe estar compuesto de 3 caracteres obligatoriamente.", ExcepcionEnum.MODELO);
        }
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = getUtilText().applyTrim(nombre);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = getUtilText().applyTrim(descripcion);
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public Calendar getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Calendar fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public int getCantMaxJugadoresPorEquipo() {
        return cantMaxJugadoresPorEquipo;
    }

    public void setCantMaxJugadoresPorEquipo(int cantMaxJugadoresPorEquipo) {
        this.cantMaxJugadoresPorEquipo = cantMaxJugadoresPorEquipo;
    }

    public int getCantMinJugadoresPorEquipo() {
        return cantMinJugadoresPorEquipo;
    }

    public void setCantMinJugadoresPorEquipo(int cantMinJugadoresPorEquipo) {
        this.cantMinJugadoresPorEquipo = cantMinJugadoresPorEquipo;
    }

    public List<Equipo> getEquipos() {
        return equipos;
    }

    public void setEquipos(List<Equipo> equipos) {
        this.equipos = equipos;
    }

    public Calendar getLimitefechaNacimiento() {
        return limitefechaNacimiento;
    }

    public void setLimitefechaNacimiento(Calendar limitefechaNacimiento) {
        this.limitefechaNacimiento = limitefechaNacimiento;
    }

    public Deporte getDeporte() {
        return deporte;
    }

    public void setDeporte(Deporte deporte) {
        this.deporte = deporte;
    }
}