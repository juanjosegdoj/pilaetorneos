package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;

import static co.uco.edu.pilae.torneos.utilitarios.cadenas.UtilText.getUtilText;
import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;


public class Genero {

    private Long codigo;

    private String nombre;

    public void validarIntegridadGenero(OperacionEnum operacion){

        switch (operacion){
            case CREAR:
                validarIntegridadNombre();
                break;

            case CONSULTAR:
                validarIntegridadCodigo();
                break;

            case ACTUALIZAR:
                validarIntegridadCodigo();
                validarIntegridadNombre();
                break;

            default:
                throw AplicacionExcepcion.CREAR("La operación seleccionada para validar el género No existe", ExcepcionEnum.MODELO);
            }
    }

    private void validarIntegridadCodigo(){
        if(getUtilObject().objetoEsNulo(codigo) || codigo <= 0){
            throw AplicacionExcepcion.CREAR("El código del torneo tiene que ser mayor a cero",
                    ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadNombre(){

        if(getUtilText().isEmptyOrNull(nombre) || nombre.length() < 3){
            throw AplicacionExcepcion.CREAR("El nombre del torneo es obligatorio y debe tener" +
                    "una cantidad mínima de 3 caracteres.", ExcepcionEnum.MODELO);
        }
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = getUtilText().applyTrim(nombre);
    }
}
