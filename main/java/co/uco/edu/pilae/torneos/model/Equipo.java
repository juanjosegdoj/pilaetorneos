package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;

import java.util.List;

import static co.uco.edu.pilae.torneos.utilitarios.cadenas.UtilText.getUtilText;

public class Equipo {

    private Long codigo;
    private String nombre;
    private List<Jugador> jugadores;
    private Torneo torneo;

    public void validarIntegridadEquipo(OperacionEnum operacion){

        switch (operacion)  {

            case CREAR:
                validarIntegridadNombre();
                break;

            case CONSULTAR:
            case ELIMINAR:
                validarIntegridadCodigo();
                break;

            case ACTUALIZAR:
                validarIntegridadCodigo();
                validarIntegridadNombre();
                break;

            default:
                throw AplicacionExcepcion.CREAR("La operación seleccionada para la validación del Equipo, no existe",
                        ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadNombre(){
        if(getUtilText().isEmptyOrNull(nombre) || nombre.length() < 3 ){
            throw AplicacionExcepcion.CREAR("El nombre del deporte es Obligatorio" +
                    "y debe contener un mínimo de 3 caracteres", ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadCodigo(){
        if(getUtilObject().objetoEsNulo(codigo) || codigo <= 0){
            throw AplicacionExcepcion.CREAR("El codigo del deporte debe ser mayor a 0");
        }
    }

    public Equipo() {
        super();
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = getUtilText().applyTrim(nombre);
    }

    public List<Jugador> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<Jugador> jugadores) {
        this.jugadores = jugadores;
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }
}
