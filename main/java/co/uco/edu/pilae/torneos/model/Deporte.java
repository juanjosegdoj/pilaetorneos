package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;

import static co.uco.edu.pilae.torneos.utilitarios.cadenas.UtilText.getUtilText;
import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;

public class Deporte {

    private Long codigo;
    private String nombre;
    private String descripcion;

    public Deporte() {
        super();
    }

    public void validarIntegridadDeporte(OperacionEnum operacion){

        switch (operacion)  {

            case ACTUALIZAR:
            case CREAR:
                validarIntegridadNombre();
                break;

            case ELIMINAR:
            case CONSULTAR:
                validarIntegridadCodigo();
                break;

             default:
                 throw AplicacionExcepcion.CREAR("La Operación seleccionada no existe.", ExcepcionEnum.MODELO);
        }
    }
    private void validarIntegridadNombre(){
        if(getUtilText().isEmptyOrNull(nombre) || nombre.length() < 3 ){
            throw AplicacionExcepcion.CREAR("El nombre del deporte es Obligatorio" +
                    "y debe contener un mínimo de 3 caracteres", ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadCodigo(){
        if(getUtilObject().objetoEsNulo(codigo) || codigo <= 0){
            throw AplicacionExcepcion.CREAR("El codigo del deporte debe ser mayor a 0");
        }
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = getUtilText().applyTrim(nombre);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = getUtilText().applyTrim(descripcion);
    }
}
