package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;

import static co.uco.edu.pilae.torneos.utilitarios.fecha.UtilDate.getUtilDate;
import static co.uco.edu.pilae.torneos.utilitarios.cadenas.UtilText.getUtilText;
import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;

import java.util.Calendar;

public class Jugador {

    private Long codigo;
    private String nombre;
    private String apellido;
    private Genero genero;
    private Calendar fechaNacimiento;
    private String identificacion;

    public Jugador() {
        super();
    }

    public void validarIntegridadJugador(OperacionEnum operacion){

        switch (operacion){

            case ELIMINAR:
            case CONSULTAR:
                validarIntegridadCodigo();
                break;

            case CREAR:
                validarIntegridadNombre();
                validarIntegridadApellido();
                validarIntegridadIdentificacion();
                validarIntegridadGenero();
                validarIntegridadFechaNacimiento();
                break;

            case ACTUALIZAR:
                validarIntegridadCodigo();
                validarIntegridadNombre();
                validarIntegridadApellido();
                validarIntegridadIdentificacion();
                validarIntegridadGenero();
                validarIntegridadFechaNacimiento();
                break;
            default:
                throw AplicacionExcepcion.CREAR("La operación seleccionada para validar el jugador no existe",
                        ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadGenero(){

        if(getUtilObject().objetoEsNulo(genero)){
            final String mensaje = "El género es obligatorio.";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }
        genero.validarIntegridadGenero(OperacionEnum.CONSULTAR);
    }

    private void validarIntegridadIdentificacion() {

        if(getUtilText().isEmptyOrNull(identificacion) || identificacion.length() < 5){
            final String mensaje = "La identificación deber tener al menos 5 caracteres.";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }

        if(identificacion.contains(" ")){
            final String mensaje = "La identificación no puede contener ningún espacio.";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadApellido() {
        if (getUtilObject().objetoEsNulo(apellido) || apellido.length() < 3) {
            final String mensaje = "El apellido del jugador es obligatorio y debe tener como mínimo 3 caracteres.";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadNombre() {
        if (getUtilText().isEmptyOrNull(nombre) || nombre.length() < 3) {
            final String mensaje = "El nombre del jugador debe tener como mínimo 3 caracteres.";

            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }
    }

    private void validarIntegridadCodigo(){
        if (getUtilObject().objetoEsNulo(codigo) || codigo < 1) {
            final String mensaje = "El codigo del JugadorEntity es obligatorio y debe ser mayor a cero";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }
    }

    public void validarIntegridadFechaNacimiento(){

        if(getUtilObject().objetoEsNulo(fechaNacimiento)){
            final String mensaje = "La fecha de nacimiento del jugador es requerida.";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }

        if(fechaNacimiento.after(Calendar.getInstance())){
            final String mensaje = "Error, la fecha de nacimiento supera a la fecha actual.";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }

        if(!getUtilDate().fueHaceMasDeUnAnio(fechaNacimiento)){
            final String mensaje = "El jugador debe tener al menos un año de nacimiento.";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }

    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = getUtilText().applyTrim(nombre);
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = getUtilText().applyTrim(apellido);
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        if (getUtilObject().objetoEsNulo(genero)) {
            final String mensaje = "El Jugador debe tener un género asignado.";
            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.MODELO);
        }
        this.genero = genero;
    }

    public Calendar getFechaNacimiento() {

        return fechaNacimiento;
    }

    public void setFechaNacimiento(Calendar fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = getUtilText().applyTrim(identificacion);
    }
}
