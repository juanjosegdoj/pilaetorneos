package co.uco.edu.pilae.torneos.repository;

import co.uco.edu.pilae.torneos.entity.JugadorEntity;
import co.uco.edu.pilae.torneos.model.Jugador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.List;

public interface JugadorRepository extends JpaRepository<JugadorEntity, Serializable> {

    @Query("SELECT jug FROM JugadorEntity jug JOIN jug.equipos eq WHERE eq.torneo.codigo = ?1 AND jug.identificacion =?2")
    JugadorEntity findByEquipo_CodigoAndIdentificacion(Long codTorneo, String identificacion);

    JugadorEntity findByIdentificacion(String identificacion);

    @Query("SELECT jug FROM JugadorEntity jug JOIN jug.equipos eq WHERE eq.torneo.codigo = ?1")
    List<JugadorEntity> findByTorneo(Long codTorneo);
}
