package co.uco.edu.pilae.torneos.repository;

import co.uco.edu.pilae.torneos.entity.TorneoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface TorneoRepository extends JpaRepository<TorneoEntity, Serializable> {

    TorneoEntity findByNombre(String nombre);

    boolean existsByCodigo(Long codigo);

}
