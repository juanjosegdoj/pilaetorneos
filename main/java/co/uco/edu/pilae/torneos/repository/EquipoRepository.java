package co.uco.edu.pilae.torneos.repository;

import co.uco.edu.pilae.torneos.entity.EquipoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.List;

public interface EquipoRepository extends JpaRepository<EquipoEntity, Serializable> {

    @Query("SELECT eq FROM EquipoEntity eq JOIN eq.jugadores jug WHERE eq.torneo.codigo = ?1 AND jug.identificacion =?2")
    EquipoEntity findByTorneo_codigoAndJugador_identificacion(Long codTorneo, String identificacion);

    @Query("SELECT count(jug) FROM JugadorEntity as jug JOIN jug.equipos as eq WHERE eq.codigo =?1")
    int countJugadoresByEquipo_Codigo(Long codigo);

    @Query("FROM EquipoEntity as eq WHERE eq.nombre=?1 and eq.torneo.codigo =?2")
    EquipoEntity findByTorneoYEquipo(String nombreEquipo, Long codTorneo);

    List<EquipoEntity> findByTorneo_Codigo(Long coTorneo);
}
