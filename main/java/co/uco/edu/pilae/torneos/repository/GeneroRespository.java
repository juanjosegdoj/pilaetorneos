package co.uco.edu.pilae.torneos.repository;

import co.uco.edu.pilae.torneos.entity.GeneroEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;

public interface GeneroRespository extends JpaRepository<GeneroEntity, Serializable> {

    @Query("FROM GeneroEntity as gen WHERE gen.nombre=?1")
    GeneroEntity findByName(String nombreCategoria);
}
