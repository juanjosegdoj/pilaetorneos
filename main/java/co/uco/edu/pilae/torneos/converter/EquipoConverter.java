package co.uco.edu.pilae.torneos.converter;

import co.uco.edu.pilae.torneos.entity.EquipoEntity;
import co.uco.edu.pilae.torneos.model.Equipo;
import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;

import java.util.ArrayList;
import java.util.List;

@Component
public class EquipoConverter {

    @Autowired
    private JugadorConverter jugadorConverter;

    @Autowired
    private TorneoConverter torneoConverter;

    public EquipoEntity modelToEntity(Equipo equipo, OperacionEnum operacion) {

        equipo.validarIntegridadEquipo(operacion);

        EquipoEntity equipoEntity = null;

        if (!getUtilObject().objetoEsNulo(equipo)) {
            equipoEntity = new EquipoEntity();

            equipoEntity.setCodigo(equipo.getCodigo());
            equipoEntity.setNombre(equipo.getNombre());
            if(!getUtilObject().objetoEsNulo(equipo.getJugadores())){
                equipoEntity.setJugadores(jugadorConverter.modelToEntity(equipo.getJugadores()));
            }
        }
        return equipoEntity;
    }

    public Equipo entityToModel(EquipoEntity equipoEntity) {

        Equipo equipo = null;

        if (!getUtilObject().objetoEsNulo(equipoEntity)) {

            equipo = new Equipo();

            equipo.setCodigo(equipoEntity.getCodigo());
            equipo.setNombre(equipoEntity.getNombre());
            equipo.setJugadores(jugadorConverter.entityToModel(equipoEntity.getJugadores()));
            equipo.setTorneo(torneoConverter.entityToModel(equipoEntity.getTorneo()));
        }

        return equipo;
    }

    public List<Equipo> entityToModel(List<EquipoEntity> equipoEntityList) {

        List<Equipo> equipos = new ArrayList<>(equipoEntityList.size());

        equipoEntityList.forEach(
                entity -> equipos.add(entityToModel(entity))
        );
        return equipos;
    }
}
