package co.uco.edu.pilae.torneos.converter;

import co.uco.edu.pilae.torneos.entity.GeneroEntity;
import co.uco.edu.pilae.torneos.model.Genero;
import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import lombok.Data;
import org.springframework.stereotype.Component;
import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;

import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class GeneroConverter {

    public GeneroEntity modelToEntity(Genero genero, OperacionEnum operacion) {

        genero.validarIntegridadGenero(operacion);

        GeneroEntity generoEntity = null;

        if(!getUtilObject().objetoEsNulo(genero)){
            generoEntity = new GeneroEntity();

            generoEntity.setCodigo(genero.getCodigo());
            generoEntity.setNombre(genero.getNombre());
        }

        return generoEntity;
    }

    public Genero entityToModel(GeneroEntity generoEntity) {
        Genero genero = null;

        if (!getUtilObject().objetoEsNulo(generoEntity)) {
            genero = new Genero();

            genero.setCodigo(generoEntity.getCodigo());
            genero.setNombre(generoEntity.getNombre());
        }


        return genero;
    }

    public List<Genero> entityToModel(List<GeneroEntity> generosEntity) {
        List<Genero> generoEntities = new ArrayList<>(generosEntity.size());

        generosEntity.forEach(entity ->
            generoEntities.add(entityToModel(entity))
        );

        return generoEntities;
    }

}
