package co.uco.edu.pilae.torneos.converter;

import co.uco.edu.pilae.torneos.entity.JugadorEntity;
import static  co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;
import co.uco.edu.pilae.torneos.model.Jugador;
import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class JugadorConverter {

    @Autowired
    public GeneroConverter generoConverter;

    public JugadorEntity modelToEntity(Jugador jugador, OperacionEnum operacion) {

        jugador.validarIntegridadJugador(operacion);

        JugadorEntity jugadorEntity = null;

        if (!getUtilObject().objetoEsNulo(jugador)){

            jugadorEntity = new JugadorEntity();

            jugadorEntity.setCodigo(jugador.getCodigo());
            jugadorEntity.setNombre(jugador.getNombre());
            jugadorEntity.setApellido(jugador.getApellido());
            jugadorEntity.setFechaNacimiento(jugador.getFechaNacimiento());
            jugadorEntity.setIdentificacion(jugador.getIdentificacion());
            jugadorEntity.setGeneroEntity(generoConverter.modelToEntity(jugador.getGenero(), OperacionEnum.CONSULTAR));

        }
        return jugadorEntity;
    }

    public Jugador entityToModel(JugadorEntity jugadorEntity) {
        Jugador jugador;

        if (jugadorEntity == null) {
            jugador = null;
        } else {
            jugador = new Jugador();

            jugador.setCodigo(jugadorEntity.getCodigo());
            jugador.setNombre(jugadorEntity.getNombre());
            jugador.setApellido(jugadorEntity.getApellido());
            jugador.setFechaNacimiento(jugadorEntity.getFechaNacimiento());
            jugador.setIdentificacion(jugadorEntity.getIdentificacion());
            jugador.setGenero(generoConverter.entityToModel(jugadorEntity.getGeneroEntity()));
        }

        return jugador;

    }

    public List<Jugador> entityToModel(List<JugadorEntity> jugadorEntities) {
        List<Jugador> jugadores = new ArrayList<>(jugadorEntities.size());

        jugadorEntities.forEach(
                entity -> jugadores.add(entityToModel(entity))
        );

        return jugadores;
    }

    public List<JugadorEntity> modelToEntity(List<Jugador> jugadores) {

        System.out.println(jugadores);
        List<JugadorEntity> jugadorEntities = new ArrayList<>(jugadores.size());

        jugadores.forEach(
                model -> jugadorEntities.add(modelToEntity(model, OperacionEnum.POBLAR))
        );

        return jugadorEntities;

    }
}
