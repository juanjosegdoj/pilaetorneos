package co.uco.edu.pilae.torneos.converter;

import co.uco.edu.pilae.torneos.entity.TorneoEntity;
import co.uco.edu.pilae.torneos.model.Torneo;
import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TorneoConverter {

    @Autowired
    private GeneroConverter generoConverter;

    @Autowired
    private DeporteConverter deporteConverter;

    public TorneoEntity modelToEntity(Torneo torneo, OperacionEnum operacion){

        torneo.validarIntegridadTorneo(operacion);

        TorneoEntity torneoEntity = new TorneoEntity();

        switch (operacion){

            case CREAR:
                torneoEntity.setNombre(torneo.getNombre());
                torneoEntity.setDescripcion(torneo.getDescripcion());
                torneoEntity.setGenero(generoConverter.modelToEntity(torneo.getGenero(), OperacionEnum.CONSULTAR));
                torneoEntity.setFechaInicio(torneo.getFechaInicio());
                torneoEntity.setCantMaxJugadoresPorEquipo(torneo.getCantMaxJugadoresPorEquipo());
                torneoEntity.setCantMinJugadoresPorEquipo(torneo.getCantMinJugadoresPorEquipo());
                torneoEntity.setDeporte(deporteConverter.modelToEntity(torneo.getDeporte(),OperacionEnum.CONSULTAR));
                torneoEntity.setLimitefechaNacimiento(torneo.getLimitefechaNacimiento());
                break;

            case CONSULTAR:
                torneoEntity.setCodigo(torneo.getCodigo());
                break;

            case ACTUALIZAR:
                torneoEntity.setCodigo(torneo.getCodigo());
                torneoEntity.setNombre(torneo.getNombre());
                torneoEntity.setDescripcion(torneo.getDescripcion());
                torneoEntity.setDeporte(deporteConverter.modelToEntity(torneo.getDeporte(), OperacionEnum.CONSULTAR));
                torneoEntity.setGenero(generoConverter.modelToEntity(torneo.getGenero(), OperacionEnum.CONSULTAR));
                torneoEntity.setFechaInicio(torneo.getFechaInicio());
                torneoEntity.setCantMaxJugadoresPorEquipo(torneo.getCantMaxJugadoresPorEquipo());
                torneoEntity.setCantMinJugadoresPorEquipo(torneo.getCantMinJugadoresPorEquipo());
                torneoEntity.setLimitefechaNacimiento(torneo.getLimitefechaNacimiento());
                break;
            default:
                throw AplicacionExcepcion.CREAR("La operación seleccionada no existe.", ExcepcionEnum.MODELO);
        }
        return torneoEntity;
    }

    public Torneo entityToModel(TorneoEntity torneoEntity){
        Torneo torneo;

        if(!getUtilObject().objetoEsNulo(torneoEntity)){
            torneo = new Torneo();

            torneo.setCodigo(torneoEntity.getCodigo());
            torneo.setNombre(torneoEntity.getNombre());
            torneo.setDescripcion(torneoEntity.getDescripcion());
            torneo.setDeporte(deporteConverter.entityToModel(torneoEntity.getDeporte()));
            torneo.setGenero(generoConverter.entityToModel(torneoEntity.getGenero()));
            torneo.setFechaInicio(torneoEntity.getFechaInicio());
            torneo.setCantMaxJugadoresPorEquipo(torneoEntity.getCantMaxJugadoresPorEquipo());
            torneo.setCantMinJugadoresPorEquipo(torneoEntity.getCantMinJugadoresPorEquipo());
            torneo.setLimitefechaNacimiento(torneoEntity.getLimitefechaNacimiento());
        }else {
            torneo = null;
        }

        return torneo;
    }

    public List<Torneo> entityToModel(List<TorneoEntity> torneosEntity){
        List<Torneo> torneos = new ArrayList<>(torneosEntity.size());

        torneosEntity.forEach(entity->
            torneos.add(entityToModel(entity))
        );

        return torneos;
    }
}
