package co.uco.edu.pilae.torneos.converter;

import co.uco.edu.pilae.torneos.entity.DeporteEntity;
import co.uco.edu.pilae.torneos.model.Deporte;
import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DeporteConverter {

    public DeporteEntity modelToEntity(Deporte deporte, OperacionEnum operacion) {

        deporte.validarIntegridadDeporte(operacion);

        DeporteEntity deporteEntity = new DeporteEntity();

        switch (operacion)  {

            case ACTUALIZAR:
                deporteEntity.setCodigo(deporte.getCodigo());
                deporteEntity.setNombre(deporte.getNombre());
                deporteEntity.setDescripcion(deporte.getDescripcion());
                break;

            case CREAR:
                deporteEntity.setNombre(deporte.getNombre());
                deporteEntity.setDescripcion(deporte.getDescripcion());
                break;

            case CONSULTAR:
            case ELIMINAR:
                deporteEntity.setCodigo(deporte.getCodigo());
                break;
             default:
                 throw AplicacionExcepcion.CREAR("La operación seleccionada no existe", ExcepcionEnum.MODELO);
        }

        return deporteEntity;
    }

    public Deporte entityToModel(DeporteEntity deporteEntity) {
        Deporte deporte = null;

        if (deporteEntity != null) {
            deporte = new Deporte();
            deporte.setCodigo(deporteEntity.getCodigo());
            deporte.setNombre(deporteEntity.getNombre());
            deporte.setDescripcion(deporteEntity.getDescripcion());
        }

        return deporte;
    }

    public List<Deporte> entityToModel(List<DeporteEntity> deportesEntity) {
        List<Deporte> deportes = new ArrayList<>(deportesEntity.size());

        deportesEntity.forEach(model -> deportes.add(entityToModel(model)));

        return deportes;
    }

}
