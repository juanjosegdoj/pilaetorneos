package co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion;

import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;

import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;
import static co.uco.edu.pilae.torneos.utilitarios.cadenas.UtilText.getUtilText;


public final class AplicacionExcepcion extends RuntimeException {

    /**
     * Serial Version por defecto de la clase
     */
    private static final long serialVersionUID = 1L;

    private String mensaje;
    private Exception excepcionRaiz;
    private ExcepcionEnum lugarExcepcion;

    private AplicacionExcepcion(final String mensaje, final Exception excepcionRaiz,
                                final ExcepcionEnum lugarExcepcion) {
        super();
        setMensaje(mensaje);
        setExcepcionRaiz(excepcionRaiz);
        setLugarExcepcion(lugarExcepcion);
    }


    public static AplicacionExcepcion CREAR(final String mensaje,
                                            final Exception excepcionRaiz,
                                            final ExcepcionEnum lugarExcepcion) {

        return new AplicacionExcepcion(mensaje, excepcionRaiz, lugarExcepcion);
    }


    public static AplicacionExcepcion CREAR(final String mensaje, final ExcepcionEnum lugarExcepcion) {
        return new AplicacionExcepcion(mensaje, null, lugarExcepcion);
    }

    public static AplicacionExcepcion CREAR(final String mensaje) {
        return new AplicacionExcepcion(mensaje, null, null);
    }


    private final void setMensaje(String mensaje) {
        this.mensaje = getUtilText().applyTrim(mensaje);
    }

    private final void setExcepcionRaiz(Exception excepcionRaiz) {
        this.excepcionRaiz = getUtilObject().getDefectValue(excepcionRaiz, new Exception());
    }

    private final void setLugarExcepcion(ExcepcionEnum lugarExcepcion) {
        this.lugarExcepcion = getUtilObject().getDefectValue(lugarExcepcion, ExcepcionEnum.GENERAL);
    }


    public final String getMensaje() {
        return mensaje;
    }

    public final Exception getExcepcionRaiz() {
        return excepcionRaiz;
    }

    public final ExcepcionEnum getLugarExcepcion() {
        return lugarExcepcion;
    }
}