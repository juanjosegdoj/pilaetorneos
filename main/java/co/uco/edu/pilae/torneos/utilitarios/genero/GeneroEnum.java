package co.uco.edu.pilae.torneos.utilitarios.genero;

public enum GeneroEnum {

    MIXTO("Mixto");

    private String nombre;

    private GeneroEnum(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
}
