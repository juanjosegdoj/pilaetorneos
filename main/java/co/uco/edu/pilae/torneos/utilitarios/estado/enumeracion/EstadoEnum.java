
package co.uco.edu.pilae.torneos.utilitarios.estado.enumeracion;

public enum EstadoEnum {

    ENINSCRIPCION, ENJUEGO, CANCELADO, FINALIZADO;

}
