package co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion;
import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;

public enum OperacionEnum {

    CREAR, ACTUALIZAR, ELIMINAR, CONSULTAR, SIN_VALOR, POBLAR;

    public final static OperacionEnum obtenerValorDefecto(final OperacionEnum operacion) {
        return getUtilObject().getDefectValue(operacion, OperacionEnum.SIN_VALOR);
    }

}
