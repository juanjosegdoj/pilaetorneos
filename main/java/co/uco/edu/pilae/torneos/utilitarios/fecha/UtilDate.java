package co.uco.edu.pilae.torneos.utilitarios.fecha;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class UtilDate {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private static final UtilDate INSTANCE = new UtilDate();

    private UtilDate() {
        super();
    }

    public static final UtilDate getUtilDate() {
        return INSTANCE;
    }

    public Calendar toCalendar(String fechaStr) throws ParseException {

        Calendar fechaCalendar = Calendar.getInstance();
        fechaCalendar.setTime(sdf.parse(fechaStr));

        return fechaCalendar;
    }

    public boolean fueHaceMasDeUnAnio(Calendar calendar){

        boolean haceMasDeUnAnio = false;

        Calendar fechaActualMenosUnAnio = Calendar.getInstance();
        fechaActualMenosUnAnio.set(Calendar.YEAR, fechaActualMenosUnAnio.get(Calendar.YEAR)-1);

        if(calendar.before(fechaActualMenosUnAnio)){
            haceMasDeUnAnio = true;
        }
        return haceMasDeUnAnio;
    }
    public String toString(Calendar fecha) {
        return this.sdf.format(fecha.getTime());
    }

    public boolean areEquals(Date fecha1, Calendar fecha2) {
        return (this.sdf.format(fecha1.getTime()).equals(sdf.format(fecha2.getTime())));

    }

}
