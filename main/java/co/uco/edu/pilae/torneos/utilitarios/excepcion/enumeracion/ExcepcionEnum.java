package co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion;


public enum ExcepcionEnum {

    SERVICIO("SERVICIO", "Excepcion presentada en la capa de Servicio"),
    MODELO("MODELO", "Excepcion presentada en la capa del modelo."),
    GENERAL("GENERAL", "Excepcion presentada a nivel General"),
    ENTIDAD("ENTIDAD", "Excepcion presentada en el ENTIDAD");


    private String codigo;
    private String nombre;

    private ExcepcionEnum(final String codigo, final String nombre) {
        setCodigo(codigo);
        setNombre(nombre);
    }

    public final String getCodigo() {
        return codigo;
    }

    private final void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public final String getNombre() {
        return nombre;
    }

    private final void setNombre(String nombre) {
        this.nombre = nombre;
    }


}
