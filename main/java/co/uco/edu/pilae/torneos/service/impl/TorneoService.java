package co.uco.edu.pilae.torneos.service.impl;

import co.uco.edu.pilae.torneos.converter.TorneoConverter;
import co.uco.edu.pilae.torneos.entity.DeporteEntity;
import co.uco.edu.pilae.torneos.entity.GeneroEntity;
import co.uco.edu.pilae.torneos.entity.TorneoEntity;
import co.uco.edu.pilae.torneos.model.Torneo;
import co.uco.edu.pilae.torneos.repository.DeporteRepository;
import co.uco.edu.pilae.torneos.repository.GeneroRespository;
import co.uco.edu.pilae.torneos.repository.TorneoRepository;
import co.uco.edu.pilae.torneos.service.ITorneoService;
import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;

@Service
public class TorneoService implements ITorneoService {

    @Autowired
    private TorneoRepository torneoRepository;

    @Autowired
    private TorneoConverter torneoConverter;

    @Autowired
    private DeporteRepository deporteRepository;

    @Autowired
    private GeneroRespository generoRespository;

    @Override
    public void save(Torneo torneo) {

        // 1ra Validación, idenficar que existan el deporte y género ingresado
        Optional<DeporteEntity> deporte = deporteRepository.findById(torneo.getDeporte().getCodigo());
        if(!deporte.isPresent()){
            throw AplicacionExcepcion.CREAR("No existe el deporte seleccioando, por lo que no se puede crear el torneo", ExcepcionEnum.SERVICIO);
        }

        Optional<GeneroEntity> genero = generoRespository.findById(torneo.getGenero().getCodigo());
        if(!genero.isPresent()){
            throw AplicacionExcepcion.CREAR("No se encontró el género seleccionado, por lo que no se puede crear el torneo");
        }

        TorneoEntity reglaTres = torneoRepository.findByNombre(torneo.getNombre());
        if(!getUtilObject().objetoEsNulo(reglaTres)){
            throw AplicacionExcepcion.CREAR("No es posible crear el torneo ingresado, pues el torneo "+
                    reglaTres.getNombre()+" ya existe en la plataforma.", ExcepcionEnum.SERVICIO);
        }

        torneoRepository.save(torneoConverter.modelToEntity(torneo, OperacionEnum.CREAR));
    }

    @Override
    public List<Torneo> findAll() {
        return torneoConverter.entityToModel(torneoRepository.findAll());
    }

    @Override
    public Torneo findById(Long codigo) {
        return torneoConverter.entityToModel(torneoRepository.findById(codigo).orElse(null));
    }

    @Override
    public void eliminar(Long codigo){
        if(torneoRepository.existsByCodigo(codigo)){
            torneoRepository.deleteById(codigo);
        }else {
            throw AplicacionExcepcion.CREAR("No es posible eliminar un Torneo inexistente.");
        }
    }

    @Override
    public void actualizar(Torneo torneo) {

        // Validando la integridad de los campos, así no vaya a usar el objeto convertiodo.
        torneoConverter.modelToEntity(torneo, OperacionEnum.ACTUALIZAR);

        // 1r Validación, idenficar que existan el Torneo
        Optional<TorneoEntity> torneoEntity = torneoRepository.findById(torneo.getCodigo());

        if(!torneoEntity.isPresent()){
            throw AplicacionExcepcion.CREAR("Error, no es posible actualizar un torneo inexistente.");
        }

        // ReglaDos: Verificar que el posible nuevo nombre del torneo, no esté siendo usado en otro torneo
        TorneoEntity reglaDos = torneoRepository.findByNombre(torneo.getNombre());

        if(!getUtilObject().objetoEsNulo(reglaDos) && reglaDos.getCodigo() != torneo.getCodigo()){
            throw AplicacionExcepcion.CREAR("No es posible crear el torneo ingresado, pues el torneo " +
                    "ya existe "+ reglaDos.getNombre()+" en la plataforma.", ExcepcionEnum.SERVICIO);
        }else {
            torneoEntity.get().setGenero(torneoEntity.get().getGenero());
        }

        // ReglaTres: Verifica que el posible género y deporte nuevos existan.
        if(torneoEntity.get().getDeporte().getCodigo() != torneo.getDeporte().getCodigo()){
            Optional<DeporteEntity> deporte = deporteRepository.findById(torneo.getDeporte().getCodigo());

            if(!deporte.isPresent()){
                throw AplicacionExcepcion.CREAR("Error, No existe el deporte seleccionado",
                        ExcepcionEnum.SERVICIO);
            }else {
                torneoEntity.get().setDeporte(deporte.get());
            }
        }

        if(torneoEntity.get().getGenero().getCodigo() != torneo.getGenero().getCodigo()){
            Optional<GeneroEntity> genero = generoRespository.findById(torneo.getGenero().getCodigo());

            if(!genero.isPresent()){
                throw AplicacionExcepcion.CREAR("Error, No existe el genero seleccioando,",
                        ExcepcionEnum.SERVICIO);
            }else {
                torneoEntity.get().setGenero(genero.get());
            }
        }

        // Finalmente setteo los otros atributos
        torneoEntity.get().setNombre(torneo.getNombre());
        torneoEntity.get().setDescripcion(torneo.getDescripcion());
        torneoEntity.get().setFechaInicio(torneo.getFechaInicio());
        torneoEntity.get().setLimitefechaNacimiento(torneo.getLimitefechaNacimiento());
        torneoEntity.get().setCantMinJugadoresPorEquipo(torneo.getCantMinJugadoresPorEquipo());
        torneoEntity.get().setCantMaxJugadoresPorEquipo(torneo.getCantMaxJugadoresPorEquipo());

        torneoRepository.save(torneoEntity.get());
    }


}