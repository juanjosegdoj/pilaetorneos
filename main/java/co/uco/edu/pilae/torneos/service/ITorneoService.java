package co.uco.edu.pilae.torneos.service;

import co.uco.edu.pilae.torneos.model.Equipo;
import co.uco.edu.pilae.torneos.model.Torneo;

import java.util.List;

public interface ITorneoService {

    void save(Torneo torneo);

    List<Torneo> findAll();

    Torneo findById(Long codigo);

    void actualizar(Torneo torneo);

    void eliminar(Long codigo);
}
