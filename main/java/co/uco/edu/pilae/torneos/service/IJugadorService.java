package co.uco.edu.pilae.torneos.service;

import co.uco.edu.pilae.torneos.model.Jugador;

import java.util.List;

public interface IJugadorService {

    List<Jugador> listAll();

    Jugador findById(Long id);

    void save(Jugador jugador, Long codEquipo);

    List<Jugador> findJugadores(Long codEquipo);

    void agruparJugadorAEquipo(Long codEquipo, Long codJugador);

    List<Jugador> findByTorneo(Long codTorneo);
}
