package co.uco.edu.pilae.torneos.service.impl;

import co.uco.edu.pilae.torneos.converter.EquipoConverter;
import co.uco.edu.pilae.torneos.converter.JugadorConverter;
import co.uco.edu.pilae.torneos.entity.EquipoEntity;
import co.uco.edu.pilae.torneos.entity.JugadorEntity;
import co.uco.edu.pilae.torneos.entity.TorneoEntity;
import co.uco.edu.pilae.torneos.model.Equipo;
import co.uco.edu.pilae.torneos.model.Jugador;
import co.uco.edu.pilae.torneos.repository.EquipoRepository;
import co.uco.edu.pilae.torneos.repository.JugadorRepository;
import co.uco.edu.pilae.torneos.repository.TorneoRepository;
import co.uco.edu.pilae.torneos.service.IEquipoService;
import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;
import java.util.List;
import java.util.Optional;

@Service
public class EquipoService implements IEquipoService {

    @Autowired
    private EquipoRepository equipoRepository;

    @Autowired
    private EquipoConverter equipoConverter;

    @Autowired
    private TorneoRepository torneoRepository;


    @Override
    public Equipo findById(Long id) {
        return equipoConverter.entityToModel(equipoRepository.findById(id).orElse(null));
    }

    @Override
    public List<Equipo> findAll() {
        return equipoConverter.entityToModel(equipoRepository.findAll());
    }

    @Override
    public List<Equipo> findByTorneo(Long codTorneo){
        return equipoConverter.entityToModel(equipoRepository.findByTorneo_Codigo(codTorneo));
    }

    @Override
    public void insertarEquipo(Equipo equipo, Long codTorneo){

        // 1r Validación, idenficar que exista el Torneo
        Optional<TorneoEntity> torneoEntity = torneoRepository.findById(codTorneo);
        if(!torneoEntity.isPresent()){
            throw AplicacionExcepcion.CREAR("No es posible insertar un equipo a un torneo inexistente.");
        }

        // ReglaDos: Verificar que no exista otro equipo con el mismo nombre
        EquipoEntity reglaDos = equipoRepository.findByTorneoYEquipo(equipo.getNombre(), codTorneo);

        if(!getUtilObject().objetoEsNulo(reglaDos)){
            throw AplicacionExcepcion.CREAR("El torneo "+reglaDos.getTorneo().getNombre()+" ya contiene " +
                    "un equipo con el nombre "+reglaDos.getNombre(), ExcepcionEnum.SERVICIO);
        }

        EquipoEntity equipoEntity = equipoConverter.modelToEntity(equipo, OperacionEnum.CREAR);

        equipoEntity.setTorneo(torneoEntity.get());
        equipoRepository.save(equipoEntity);

    }

}
