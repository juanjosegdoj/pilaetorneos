package co.uco.edu.pilae.torneos.service.impl;

import co.uco.edu.pilae.torneos.converter.GeneroConverter;
import co.uco.edu.pilae.torneos.model.Genero;
import co.uco.edu.pilae.torneos.repository.GeneroRespository;
import co.uco.edu.pilae.torneos.service.IGeneroService;
import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GeneroService implements IGeneroService {

    @Autowired
    private GeneroRespository generoRespository;

    @Autowired
    private GeneroConverter generoConverter;


    public List<Genero> listAll() {
        return generoConverter.entityToModel(generoRespository.findAll());
    }

    public Genero findById(long id) {
        return generoConverter.entityToModel(generoRespository.findById(id).orElse(null));
    }

    @Override
    public void insertar(Genero genero) {
        generoRespository.save(generoConverter.modelToEntity(genero, OperacionEnum.CREAR));
    }

    @Override
    public Genero findByName(String nombre) {
        return generoConverter.entityToModel(generoRespository.findByName(nombre));
    }


}
