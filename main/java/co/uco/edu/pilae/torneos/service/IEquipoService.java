package co.uco.edu.pilae.torneos.service;

import co.uco.edu.pilae.torneos.model.Equipo;
import co.uco.edu.pilae.torneos.model.Jugador;

import java.util.List;

public interface IEquipoService {

    Equipo findById(Long id);

    void insertarEquipo(Equipo equipo, Long codTorneo);

    List<Equipo> findByTorneo(Long codTorneo);

    List<Equipo> findAll();
}
