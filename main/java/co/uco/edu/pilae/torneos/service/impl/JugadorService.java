package co.uco.edu.pilae.torneos.service.impl;

import co.uco.edu.pilae.torneos.converter.JugadorConverter;
import co.uco.edu.pilae.torneos.entity.EquipoEntity;
import co.uco.edu.pilae.torneos.entity.JugadorEntity;
import co.uco.edu.pilae.torneos.model.Jugador;
import co.uco.edu.pilae.torneos.repository.EquipoRepository;
import co.uco.edu.pilae.torneos.repository.JugadorRepository;
import co.uco.edu.pilae.torneos.service.IJugadorService;
import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.enumeracion.ExcepcionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import co.uco.edu.pilae.torneos.utilitarios.genero.GeneroEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static co.uco.edu.pilae.torneos.utilitarios.objeto.UtilObject.getUtilObject;

@Service
public class JugadorService implements IJugadorService {

    @Autowired
    private JugadorRepository jugadorRepository;

    @Autowired
    private JugadorConverter jugadorConverter;

    @Autowired
    private EquipoRepository equipoRepository;

    // Create
    @Override
    public void save(Jugador jugador, Long codEquipo) {

        Optional<EquipoEntity> equipo = equipoRepository.findById(codEquipo);

        validarIntegridadParaInsercionDeJugador(equipo.get(), jugador);

        if (!getUtilObject().objetoEsNulo(jugadorRepository.findByIdentificacion(jugador.getIdentificacion()))){
            throw AplicacionExcepcion.CREAR("El jugador ya se encuentra registrado en la plataforma, " +
                            "es posible agruparlo a otro equipo de diferente torneo, pero no se puede registrar nuevamente",
                    ExcepcionEnum.SERVICIO);
        }

        equipo.get().addJugador(jugadorConverter.modelToEntity(jugador, OperacionEnum.CREAR));
        equipoRepository.save(equipo.get());
    }

    @Override
    public void agruparJugadorAEquipo(Long codEquipo, Long codJugador) {

        Optional<EquipoEntity> equipo = equipoRepository.findById(codEquipo);
        Optional<JugadorEntity> jugador = jugadorRepository.findById(codJugador);

        validarIntegridadParaInsercionDeJugador(equipo.get(), jugadorConverter.entityToModel(jugador.get()));

        equipo.get().addJugador(jugador.get());
        equipoRepository.save(equipo.get());

    }

    // Read
    @Override
    public List<Jugador> listAll() {
        return jugadorConverter.entityToModel(jugadorRepository.findAll());
    }

    @Override
    public Jugador findById(Long id) {
        return jugadorConverter.entityToModel(jugadorRepository.findById(id).orElse(null));
    }

    @Override
    public List<Jugador> findJugadores(Long codEquipo) {

        Optional<EquipoEntity> equipoEntity = equipoRepository.findById(codEquipo);
        List<Jugador> jugadores;

        if (equipoEntity.isPresent()) {
            jugadores = jugadorConverter.entityToModel(equipoEntity.get().getJugadores());
        } else {
            throw AplicacionExcepcion.CREAR("No se econtró ningún equipo con el codigo ingresado.");
        }
        return jugadores;
    }

    @Override
    public List<Jugador> findByTorneo(Long codTorneo) {
        return jugadorConverter.entityToModel(jugadorRepository.findByTorneo(codTorneo));
    }


    // Update

    // delete





    // Utilitarios de la clase

    public void validarIntegridadParaInsercionDeJugador(EquipoEntity equipo, Jugador jugador){

        // ReglaUno: Que exista el Equipo al cual se ingresará el torneo
        if (getUtilObject().objetoEsNulo(equipo)){
            throw AplicacionExcepcion.CREAR("NO existe el equipo, al cual se va a ingresar le jugador ingresado."
                    , ExcepcionEnum.SERVICIO);
        }

        // reglaDos: El jugador que se ingresará no puede existir en el torneo.
        EquipoEntity reglaDos = equipoRepository.findByTorneo_codigoAndJugador_identificacion(
                equipo.getTorneo().getCodigo(),
                jugador.getIdentificacion()
        );
        if(!getUtilObject().objetoEsNulo(reglaDos)){
            String mensaje = "El jugador con identificación "+jugador.getIdentificacion()+
                    " ya se hace parte de algún equipo en el Torneo, por lo que no puede ser insertado en " +
                    "otro torneo pero no ingresado nuevamente.";

            throw AplicacionExcepcion.CREAR(mensaje, ExcepcionEnum.SERVICIO);
        }

        // ReglaTres: Cantidad Máxima de jugadores por equipo
        int cantidadDeJugadores = equipoRepository.countJugadoresByEquipo_Codigo(equipo.getCodigo());
        if(cantidadDeJugadores >= equipo.getTorneo().getCantMaxJugadoresPorEquipo()){
            throw AplicacionExcepcion.CREAR("No es posible ingresar otro jugador, pues el torneo permite un máximo de "+
                            equipo.getTorneo().getCantMaxJugadoresPorEquipo() + " jugadores por equipo."
                    , ExcepcionEnum.SERVICIO);
        }

        // ReglaCuatro: Verificar que el genero del torneo y del jugador coincidan.
        if(jugador.getGenero().getCodigo() != equipo.getTorneo().getGenero().getCodigo() &&
                GeneroEnum.MIXTO.getNombre() != equipo.getTorneo().getGenero().getNombre()){
            throw AplicacionExcepcion.CREAR("Error, El equipo al cual desea ingresar el jugador, no corresponde con " +
                    "su género", ExcepcionEnum.SERVICIO);
        }
    }
}
