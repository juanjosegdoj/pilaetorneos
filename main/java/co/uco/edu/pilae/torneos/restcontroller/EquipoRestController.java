package co.uco.edu.pilae.torneos.restcontroller;

import co.uco.edu.pilae.torneos.model.Equipo;
import co.uco.edu.pilae.torneos.service.impl.EquipoService;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/equipos")
public class EquipoRestController {

    @Autowired
    private EquipoService equipoService;

    @GetMapping("/listar")
    public List<Equipo> listAll() {
        return equipoService.findAll();
    }

    @GetMapping("/{id}")
    public Equipo fingByID(@PathVariable Long id) {
        return equipoService.findById(id);
    }

    @GetMapping("")
    public List<Equipo> getEquipos(@RequestParam Long codTorneo){
        return equipoService.findByTorneo(codTorneo);
    }

    @PostMapping("/insertar")
    public ResponseEntity<Map<String, Object>> insertarEquipo(@RequestBody Equipo equipo, @RequestParam Long codTorneo){
        Map<String, Object> response = new HashMap<>();
        HttpStatus status;
        String mensaje;
        try{

            equipoService.insertarEquipo(equipo, codTorneo);
            mensaje = "El equipo "+equipo.getNombre()+ " ha sido insertado exitosamente.";
            status = HttpStatus.CREATED;

        }catch (AplicacionExcepcion excepcion){

            mensaje =  excepcion.getMensaje();
            status = HttpStatus.BAD_REQUEST;
        }
        response.put("Mensaje", mensaje);
        return new ResponseEntity<>(response, status);



    }

}
