package co.uco.edu.pilae.torneos.restcontroller;

import co.uco.edu.pilae.torneos.model.Genero;
import co.uco.edu.pilae.torneos.service.impl.GeneroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/generos")
@CrossOrigin("http://localhost:4200")
public class GeneroRestController {

    @Autowired
    private GeneroService generoService;

    @GetMapping("/listar")
    public List<Genero> listAll() {
        return generoService.listAll();
    }

    @GetMapping("/{id}")
    public Genero findById(@PathVariable Long id) {
        return generoService.findById(id);
    }

    @PostMapping("/insertar")
    public void insertar(@RequestBody Genero genero) {
        generoService.insertar(genero);
    }
}
