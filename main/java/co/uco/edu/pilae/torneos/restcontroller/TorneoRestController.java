package co.uco.edu.pilae.torneos.restcontroller;

import co.uco.edu.pilae.torneos.model.Equipo;
import co.uco.edu.pilae.torneos.model.Torneo;
import co.uco.edu.pilae.torneos.service.impl.TorneoService;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/torneos")
@CrossOrigin("http://localhost:4200")
public class TorneoRestController {

    @Autowired
    private TorneoService torneoService;

    @PostMapping("/insertar")
    public ResponseEntity<Map<String, Object>> insertar(@RequestBody Torneo torneo){

        Map<String, Object> response = new HashMap<>();
        HttpStatus status;
        String mensaje;
        try {
            torneoService.save(torneo);
            mensaje = "El torneo "+torneo.getNombre()+ " ha sido registrado exitosamente.";
            status = HttpStatus.CREATED;

        }catch (AplicacionExcepcion excepcion){
            mensaje = excepcion.getMensaje();
            status = HttpStatus.CONFLICT;
        }
        response.put("Mensaje", mensaje);
        return new ResponseEntity<>(response, status);
    }

    @GetMapping("/listar")
    public List<Torneo> listar(){
        return torneoService.findAll();
    }

    @GetMapping("/{id}")
    public Torneo findById(@PathVariable Long id) {
        return torneoService.findById(id);
    }

    @PutMapping("/editar")
    public ResponseEntity<Map<String, Object>> actualizar(@RequestBody Torneo torneo){

        Map<String, Object> response = new HashMap<>();
        HttpStatus status;
        String mensaje;
        try {
            torneoService.actualizar(torneo);
            mensaje = "El torneo "+torneo.getNombre()+ " ha sido editado exitosamente.";
            status = HttpStatus.CREATED;

        }catch (AplicacionExcepcion excepcion){
            mensaje = excepcion.getMensaje();
            status = HttpStatus.CONFLICT;
        }
        response.put("Mensaje", mensaje);
        return new ResponseEntity<>(response, status);

    }

    @DeleteMapping("/{id}/eliminar")
    public ResponseEntity<Map<String, Object>> eliminar(@PathVariable Long id){

        Map<String, Object> response = new HashMap<>();
        HttpStatus status;
        String mensaje;

        try {
            torneoService.eliminar(id);
            mensaje = "El torneo ha sido eliminado exitosamente.";
            status = HttpStatus.OK;

        }catch (AplicacionExcepcion excepcion){
            mensaje = excepcion.getMensaje();
            status = HttpStatus.CONFLICT;
        }/*catch (Exception excepcion){
            mensaje = "Ha ocurrido un error inesperado al tratar de eliminar el torneo en la fuente de información";
            status = HttpStatus.CONFLICT;
        }*/
        response.put("Mensaje", mensaje);
        return new ResponseEntity<>(response, status);
    }

}
