package co.uco.edu.pilae.torneos.restcontroller;

import co.uco.edu.pilae.torneos.model.Deporte;
import co.uco.edu.pilae.torneos.service.impl.DeporteService;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/deportes")
@CrossOrigin("http://localhost:4200")
public class DeporteRestController {

    @Autowired
    private DeporteService deporteService;

    @GetMapping("/listar")
    public List<Deporte> listAll() {
        return deporteService.listAll();
    }

    @GetMapping("/{id}")
    public Deporte findById(@PathVariable Long id) {
        return deporteService.findById(id);
    }

    @PostMapping("/insertar")
    public ResponseEntity<Map<String, Object>> insertar(@RequestBody Deporte deporte) {

        Map<String, Object> response = new HashMap<>();
        HttpStatus status;
        String mensaje;

        try {
            deporteService.save(deporte);
            mensaje ="El deporte "+deporte.getNombre()+ " ha sido insertado exitosamente.";
            status = HttpStatus.CREATED;
        }catch (AplicacionExcepcion excepcion){
            mensaje = excepcion.getMensaje();
            status = HttpStatus.CONFLICT;
        }
        response.put("Mensaje", mensaje);
        return new ResponseEntity<>(response, status);

    }


}
