package co.uco.edu.pilae.torneos.restcontroller;


import co.uco.edu.pilae.torneos.model.Jugador;
import co.uco.edu.pilae.torneos.service.impl.JugadorService;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/jugadores")
@CrossOrigin("http://localhost:4200")
public class JugadorRestController {

    @Autowired
    private JugadorService jugadorService;

    @GetMapping("/listar")
    public List<Jugador> listar() {
        return jugadorService.listAll();
    }

    @GetMapping("")
    public List<Jugador> findByTorneo(@RequestParam Long codTorneo){
        return jugadorService.findByTorneo(codTorneo);
    }


    @PostMapping("/insertar")
    public ResponseEntity<Map<String, Object>> save(@RequestParam Long codEquipo, @RequestBody Jugador jugador){

        Map<String, Object> response = new HashMap<>();
        HttpStatus status;
        String mensaje;

        try {
            jugadorService.save(jugador, codEquipo);
            mensaje = "El jugador identificado con "+jugador.getIdentificacion()+" ha sido insertado exitosamente.";
            status = HttpStatus.CREATED;
        }catch (AplicacionExcepcion excepcion){
            mensaje = excepcion.getMensaje();
            status = HttpStatus.CONFLICT;
        }
        response.put("Mensaje", mensaje);
        return new ResponseEntity<>(response, status);
    }

}
