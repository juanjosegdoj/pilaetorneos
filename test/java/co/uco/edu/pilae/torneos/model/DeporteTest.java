package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.junit.Test;

import static org.junit.Assert.*;

public class DeporteTest {

    @Test(expected = AplicacionExcepcion.class)
    public void caracteresMenosDe3NoPermitidosEnElNombre(){

        Deporte deporte = new Deporte();
        deporte.setNombre("F");
        deporte.validarIntegridadDeporte(OperacionEnum.CREAR);
    }

    @Test
    public void noLanzaExcepciónAlIngresarValoresObligatoriosEnOperacionCREAR(){

        Deporte deporte = new Deporte();
        deporte.setCodigo(null);
        deporte.setNombre("Fútbol");
        deporte.setDescripcion(null);

        deporte.validarIntegridadDeporte(OperacionEnum.CREAR);

        assertEquals(deporte.getNombre(), deporte.getNombre());
    }

    @Test
    public void noLanzaExcepciónAlIngresarValoresMínimoDelNombreOCREAR(){

        Deporte deporte = new Deporte();
        deporte.setNombre("Vol");

        deporte.validarIntegridadDeporte(OperacionEnum.CREAR);

        assertEquals(deporte.getNombre(), deporte.getNombre());
    }

    @Test
    public void NolanzaExcepciónAlIngresarValoresObligatoriosEnCONSULTAR(){

        Deporte deporte = new Deporte();
        deporte.setCodigo((long) 1);

        deporte.validarIntegridadDeporte(OperacionEnum.CONSULTAR);

        assertEquals(deporte.getCodigo(), deporte.getCodigo());
    }

    @Test(expected = AplicacionExcepcion.class)
    public void lanzaExcepciónAlIngresarcodigoINcorrectamenteEnCONSULTAR(){

        Deporte deporte = new Deporte();
        deporte.setCodigo((long) -2);

        deporte.validarIntegridadDeporte(OperacionEnum.CONSULTAR);
        assertEquals(deporte.getNombre(), deporte.getNombre());
    }

    @Test
    public void trimAplicadoCorrectamenteEnDescripcionYNombreAlSettear(){

        // Input
        String nombre = "    Fútbol     ";
        String descripcion = "    decripción de           deporte con    un montón      de     huecos          ";

        // output esperado
        String nombreEsperado = "Fútbol";
        String descripcionEsperado = "decripción de deporte con un montón de huecos";


        Deporte deporte = new Deporte();
        deporte.setCodigo((long) -2);
        deporte.setNombre(nombre);
        deporte.setDescripcion(descripcion);

        deporte.validarIntegridadDeporte(OperacionEnum.CREAR);
        assertEquals(deporte.getNombre()+deporte.getDescripcion(), nombreEsperado+descripcionEsperado);
    }

    public void insertarUnDeporteVacio(){

    }
}