package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

public class TorneoTest {

    @Test
    public void insertarUnTorneoValidoParaOperacionConsultar() throws AplicacionExcepcion {
        // Arrange
        Torneo torneo = new Torneo();

        // Act
        torneo.setCodigo((long) 2);
        torneo.validarIntegridadTorneo(OperacionEnum.CONSULTAR);

        // Assert
        // El objetivo es que no lance error
    }

    @Test
    public void validandoFechaLimiteDeNacimientoCorrecta() throws AplicacionExcepcion{

        // Arrange
        Torneo torneo = new Torneo();
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);

        //Act
        torneo.setLimitefechaNacimiento(fechaNacimiento);

        // Assert
        torneo.validarIntegridadLimiteFechaNacimiento();
    }

    @Test(expected = AplicacionExcepcion.class)
    public void validandoFechaLimiteDeNacimientoIncorrecta() {

        // Arrange
        Torneo torneo = new Torneo();
        Calendar fechaNacimiento = Calendar.getInstance();

        //Act
        torneo.setLimitefechaNacimiento(fechaNacimiento);

        // Assert
        torneo.validarIntegridadLimiteFechaNacimiento();
    }

    @Test
    public void insertarUnTorneoValidoParaOperacionCrearConSoloDatosObligatorios() throws AplicacionExcepcion {

        //Arrange
        Torneo torneo = new Torneo();
        Genero genero = new Genero();
        genero.setCodigo((long) 2);
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Deporte deporte = new Deporte();
        deporte.setCodigo((long) 1);

        // Act
        torneo.setNombre("Asobdim");
            // descripción no es obligatoroa
        torneo.setGenero(genero);
            // fechaIncio no es obligatoria
        torneo.setCantMaxJugadoresPorEquipo(22);
        torneo.setCantMinJugadoresPorEquipo(13);
        torneo.setLimitefechaNacimiento(fechaNacimiento);
        torneo.setDeporte(deporte);

        // Assert
        torneo.validarIntegridadTorneo(OperacionEnum.CREAR);
    }

    @Test(expected = AplicacionExcepcion.class)
    public void insertarUnTorneoConCantMinYMaxDeJugadoresIncorrecta() throws AplicacionExcepcion {

        //Arrange
        Torneo torneo = new Torneo();
        Genero genero = new Genero();
        genero.setCodigo((long) 2);
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Deporte deporte = new Deporte();
        deporte.setCodigo((long) 1);

        // Act
        torneo.setNombre("Asobdim");
        torneo.setDescripcion("Asociación de barras del Medellin");
        torneo.setGenero(genero);
        torneo.setFechaInicio(Calendar.getInstance());
        torneo.setCantMaxJugadoresPorEquipo(13);
        torneo.setCantMinJugadoresPorEquipo(22);
        torneo.setLimitefechaNacimiento(fechaNacimiento);
        torneo.setDeporte(deporte);

        // Assert
        torneo.validarIntegridadTorneo(OperacionEnum.CREAR);
    }

    @Test(expected = AplicacionExcepcion.class)
    public void insertarUnTorneoConGeneroVacio() throws AplicacionExcepcion {

        //Arrange
        Torneo torneo = new Torneo();
        Genero genero = new Genero();
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Deporte deporte = new Deporte();
        deporte.setCodigo((long) 1);

        // Act
        torneo.setNombre("Asobdim");
        torneo.setGenero(genero);
        torneo.setFechaInicio(Calendar.getInstance());
        torneo.setCantMaxJugadoresPorEquipo(22);
        torneo.setCantMinJugadoresPorEquipo(13);
        torneo.setDeporte(deporte);

        // Assert
        torneo.validarIntegridadTorneo(OperacionEnum.CREAR);
    }

    @Test(expected = AplicacionExcepcion.class)
    public void insertarUnTorneoConDeporteVacio() throws AplicacionExcepcion {

        //Arrange
        Torneo torneo = new Torneo();
        Genero genero = new Genero();
        genero.setCodigo((long) 2);
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Deporte deporte = new Deporte();

        // Act
        torneo.setNombre("Asobdim");
        torneo.setDescripcion("Asociación de barras del Medellin");
        torneo.setGenero(genero);
        torneo.setFechaInicio(Calendar.getInstance());
        torneo.setCantMaxJugadoresPorEquipo(22);
        torneo.setCantMinJugadoresPorEquipo(13);
        torneo.setLimitefechaNacimiento(fechaNacimiento);
        torneo.setDeporte(deporte);

        // Assert
        torneo.validarIntegridadTorneo(OperacionEnum.CREAR);
    }

    @Test(expected = AplicacionExcepcion.class)
    public void insertarUnTorneoConLimiteFechaDeNacimientoNoPermitida() {

        //Arrange
        Torneo torneo = new Torneo();
        Genero genero = new Genero();
        genero.setCodigo((long) 2);
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)+2);
        Deporte deporte = new Deporte();

        // Act
        torneo.setNombre("Asobdim");
        torneo.setDescripcion("Asociación de barras del Medellin");
        torneo.setGenero(genero);
        torneo.setFechaInicio(Calendar.getInstance());
        torneo.setCantMaxJugadoresPorEquipo(22);
        torneo.setCantMinJugadoresPorEquipo(13);
        torneo.setLimitefechaNacimiento(fechaNacimiento);
        torneo.setDeporte(deporte);

        // Assert
        torneo.validarIntegridadTorneo(OperacionEnum.CREAR);
    }

    @Test(expected = AplicacionExcepcion.class)
    public void insertarUnTorneoConNombreConInsuficienteCaracteres() {

        //Arrange
        Torneo torneo = new Torneo();
        Genero genero = new Genero();
        genero.setCodigo((long) 2);
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)+2);
        Deporte deporte = new Deporte();

        // Act
        torneo.setNombre("As");
        torneo.setDescripcion("Asociación de barras del Medellin");
        torneo.setGenero(genero);
        torneo.setFechaInicio(Calendar.getInstance());
        torneo.setCantMaxJugadoresPorEquipo(22);
        torneo.setCantMinJugadoresPorEquipo(13);
        torneo.setLimitefechaNacimiento(fechaNacimiento);
        torneo.setDeporte(deporte);

        // Assert
        torneo.validarIntegridadTorneo(OperacionEnum.CREAR);
    }

    @Test
    public void insertarUnTorneoValidoParaOperacionCrearConDatosObligatoriosYOpcionales() throws AplicacionExcepcion {

        //Arrange
        Torneo torneo = new Torneo();
        Genero genero = new Genero();
        genero.setCodigo((long) 2);
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Deporte deporte = new Deporte();
        deporte.setCodigo((long) 1);

        // Act
        torneo.setNombre("Asobdim");
        torneo.setDescripcion("Asociación de barras del Medellin");
        torneo.setGenero(genero);
        torneo.setFechaInicio(Calendar.getInstance());
        torneo.setCantMaxJugadoresPorEquipo(22);
        torneo.setCantMinJugadoresPorEquipo(13);
        torneo.setLimitefechaNacimiento(fechaNacimiento);
        torneo.setDeporte(deporte);

        // Assert
        torneo.validarIntegridadTorneo(OperacionEnum.CREAR);
    }
}