package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.junit.Test;

import static org.junit.Assert.*;

public class EquipoTest {

    @Test(expected = AplicacionExcepcion.class)
    public void caracteresMenosDe3NoPermitidosEnElNombre(){

        Equipo equipo = new Equipo();
        equipo.setCodigo(null);
        equipo.setNombre("F");
        equipo.validarIntegridadEquipo(OperacionEnum.CREAR);
    }

    @Test
    public void noLanzaExcepciónAlIngresarValoresObligatoriosEnOperacionCREAR(){

        Equipo equipo = new Equipo();
        equipo.setNombre("Fútbol");
        equipo.validarIntegridadEquipo(OperacionEnum.CREAR);
        assertEquals(equipo.getNombre(), equipo.getNombre());
    }

    @Test
    public void noLanzaExcepciónAlIngresarValoresMínimoDelNombreOCREAR(){

        Equipo equipo = new Equipo();
        equipo.setNombre("Vol");

        equipo.validarIntegridadEquipo(OperacionEnum.CREAR);

        assertEquals(equipo.getNombre(), equipo.getNombre());
    }

    @Test
    public void NolanzaExcepciónAlIngresarValoresObligatoriosEnCONSULTAR(){

        Equipo equipo = new Equipo();
        equipo.setCodigo((long) 1);

        equipo.validarIntegridadEquipo(OperacionEnum.CONSULTAR);

        assertEquals(equipo.getCodigo(), equipo.getCodigo());
    }

    @Test(expected = AplicacionExcepcion.class)
    public void lanzaExcepciónAlIngresarcodigoINcorrectamenteEnCONSULTAR(){

        Deporte deporte = new Deporte();
        deporte.setCodigo((long) 0);

        deporte.validarIntegridadDeporte(OperacionEnum.CONSULTAR);
    }

    @Test
    public void trimAplicadoCorrectamenteEnDescripcionYNombreAlSettear(){

        // Input
        String nombre = "    Deportivo     Rionegro Aguilar     ";
        String descripcion = "    decripción de           equipo  Deportivo     Rionegro Aguilar        ";

        // output esperado
        String nombreEsperado = "Deportivo Rionegro Aguilar";
        String descripcionEsperado = "decripción de equipo Deportivo Rionegro Aguilar";


        Deporte deporte = new Deporte();
        deporte.setCodigo((long) -12);
        deporte.setNombre(nombre);
        deporte.setDescripcion(descripcion);

        deporte.validarIntegridadDeporte(OperacionEnum.CREAR);
        assertEquals(deporte.getNombre()+deporte.getDescripcion(), nombreEsperado+descripcionEsperado);
    }
}