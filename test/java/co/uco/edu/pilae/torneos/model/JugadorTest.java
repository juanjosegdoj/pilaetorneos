package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.junit.Test;

import java.io.StringReader;
import java.util.Calendar;

import static org.junit.Assert.*;

public class JugadorTest {

    @Test(expected = AplicacionExcepcion.class)
    public void lanzarExcepcionAlIngresarCodigoNoValidoAlConsultar(){
        Jugador jugador = new Jugador();
        jugador.setCodigo((long) -1);

        jugador.validarIntegridadJugador(OperacionEnum.CONSULTAR);
    }

    @Test
    public void codigoValido(){
        Jugador jugador = new Jugador();
        jugador.setCodigo((long) 12);
        Long esperado = (long)12;

        jugador.validarIntegridadJugador(OperacionEnum.CONSULTAR);
        assertEquals(jugador.getCodigo(), esperado);
    }

    @Test(expected = AplicacionExcepcion.class)
    public void lanzarExcepcionAlIngresarFechaDeNacimientoInvalida(){

        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.MONTH, fechaNacimiento.get(Calendar.MONTH)-1);

        Jugador jugador = new Jugador();

        jugador.setFechaNacimiento(fechaNacimiento);
        jugador.validarIntegridadFechaNacimiento();
    }


    @Test
    public void validarAlIngresarFechaDeNacimientoCorrectamente(){

        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);

        Jugador jugador = new Jugador();

        jugador.setFechaNacimiento(fechaNacimiento);
        jugador.validarIntegridadFechaNacimiento();
    }

    @Test(expected = AplicacionExcepcion.class)
    public void errorAlNoIngresarTodosLosDatosRequeridosParaCrearFaltaFechaNacimiento(){
        // Arrange
        Genero genero = new Genero();
        genero.setCodigo((long) 2);

        // Act
        Jugador jugador = new Jugador();
        jugador.setNombre("Carlos Andrés");
        jugador.setApellido("Aristizabal Parra");
        jugador.setIdentificacion("1041205025");
        jugador.setGenero(genero);
        // Note que la fechaDeNacimiento no se está ingresando

        //Assert
        jugador.validarIntegridadJugador(OperacionEnum.CREAR);
    }


    @Test(expected = AplicacionExcepcion.class)
    public void errorAlNoIngresarTodosLosDatosRequeridosParaCrearFaltaGenero(){
        // Arrange
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);

        // Act
        Jugador jugador = new Jugador();
        jugador.setNombre("Carlos Andrés");
        jugador.setApellido("Aristizabal Parra");
        jugador.setIdentificacion("1041205025");
        jugador.setFechaNacimiento(fechaNacimiento);
        // Note que el Género no se está ingresando

        //Assert
        jugador.validarIntegridadJugador(OperacionEnum.CREAR);
    }

    @Test(expected = AplicacionExcepcion.class)
    public void errorAlNoIngresarTodosLosDatosRequeridosParaCrearFaltaIdentificacación(){
        // Arrange
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Genero genero = new Genero();
        genero.setCodigo((long) 2);

        // Act
        Jugador jugador = new Jugador();
        jugador.setNombre("Carlos Andrés");
        jugador.setApellido("Aristizabal Parra");
        jugador.setFechaNacimiento(fechaNacimiento);
        jugador.setGenero(genero);
        // Notese que falta la identificación

        //Assert
        jugador.validarIntegridadJugador(OperacionEnum.CREAR);
    }

    @Test(expected = AplicacionExcepcion.class)
    public void errorAlasignarUnGeneroVacioEnElJugador(){
        // Arrange
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Genero genero = new Genero();

        // Act
        Jugador jugador = new Jugador();
        jugador.setIdentificacion("1041205025");
        jugador.setNombre("Carlos Andrés");
        jugador.setApellido("Aristizabal Parra");
        jugador.setFechaNacimiento(fechaNacimiento);
        jugador.setGenero(genero);

        //Assert
        jugador.validarIntegridadJugador(OperacionEnum.CREAR);
    }

    @Test(expected = AplicacionExcepcion.class)
    public void errorAlNoIngresarTodosLosDatosRequeridosParaCrearFaltaNombreYApellidoDeJugador(){
        // Arrange
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Genero genero = new Genero();
        genero.setCodigo((long) 2);

        // Act
        Jugador jugador = new Jugador();
        jugador.setIdentificacion("1041205025");
        // Notese que falta nombre y apellido
        jugador.setFechaNacimiento(fechaNacimiento);
        jugador.setGenero(genero);

        //Assert
        jugador.validarIntegridadJugador(OperacionEnum.CREAR);
    }

    @Test
    public void ingresandoTodosLosCamposRequeridosParaCrear() throws AplicacionExcepcion{

        // Arrange
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Genero genero = new Genero();
        genero.setCodigo((long) 2);

        // Act
        Jugador jugador = new Jugador();
        jugador.setNombre("Carlos Andrés");
        jugador.setApellido("Aristizabal Parra");
        jugador.setIdentificacion("1041205025");
        jugador.setGenero(genero);
        jugador.setFechaNacimiento(fechaNacimiento);

        //Assert
        jugador.validarIntegridadJugador(OperacionEnum.CREAR);
    }


    @Test
    public void eliminandoLosEspaciosEnIdentificacionNombreYApellido(){

        // Arrange
        Calendar fechaNacimiento = Calendar.getInstance();
        fechaNacimiento.set(Calendar.YEAR, fechaNacimiento.get(Calendar.YEAR)-2);
        Genero genero = new Genero();
        genero.setCodigo((long) 2);
        String resultadoEsperado = "Carlos Andrés" + "Aristizabal Parra" + "1041205025";

        // Act
        Jugador jugador = new Jugador();
        jugador.setNombre("    Carlos     Andrés      ");
        jugador.setApellido(" Aristizabal    Parra   ");
        jugador.setIdentificacion(" 1041205025  ");
        jugador.setGenero(genero);
        jugador.setFechaNacimiento(fechaNacimiento);
        jugador.validarIntegridadJugador(OperacionEnum.CREAR);

        //Assert
        String resultado = jugador.getNombre()+jugador.getApellido()+jugador.getIdentificacion();
        assertEquals(resultado, resultadoEsperado);
    }
}