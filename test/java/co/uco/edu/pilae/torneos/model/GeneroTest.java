package co.uco.edu.pilae.torneos.model;

import co.uco.edu.pilae.torneos.utilitarios.dominio.enumeracion.OperacionEnum;
import co.uco.edu.pilae.torneos.utilitarios.excepcion.excepcion.AplicacionExcepcion;
import org.junit.Test;

import static org.junit.Assert.*;

public class GeneroTest {

    @Test
    public void crearGeneroPermitido(){
        Genero genero = new Genero();
        genero.setNombre("Masculino");

        genero.validarIntegridadGenero(OperacionEnum.CREAR);

        assertEquals("Masculino", genero.getNombre());
    }

    @Test(expected = AplicacionExcepcion.class)
    public void lanzarExcepcionGeneroNoPermitidoEnOperacionCrear(){
        Genero genero = new Genero();
        genero.setNombre("Ma");

        genero.validarIntegridadGenero(OperacionEnum.CREAR);
    }

    @Test
    public void seElimnanLosEspaciosDelNombre(){
        Genero genero = new Genero();
        genero.setNombre("    Masculino   ");

        assertEquals("Masculino", genero.getNombre());
    }

    @Test(expected = AplicacionExcepcion.class)
    public void errorAlInsertarGeneroVacio(){
        Genero genero = new Genero();
        genero.validarIntegridadGenero(OperacionEnum.CREAR);
    }
}